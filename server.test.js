const request = require('supertest');
const app = require('./server');
const { v4: uuidv4 } = require('uuid');

const createAccount = (account_id, balance) => {
  return request(app).post('/api/accounts').send({
    account_id,
    balance
  });
};

const fetchBalance = (account_id) => {
  return request(app).get(`/api/accounts/${account_id}`).send();
};

const makeTransfer = (sender_id, recipient_id, amount) => {
  return request(app).post('/api/transfers').send({
    sender_id,
    recipient_id,
    amount
  });
};

describe('Creating accounts', () => {
  it('should create a new account', async () => {
    const res = await createAccount(uuidv4(), 100500);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      is_success: true
    });
  });

  it('should return error if the balance is negative', async () => {
    const res = await createAccount(uuidv4(), -100);
    expect(res.statusCode).toEqual(200);
    expect(res.body.is_success).toBe(false);
  });

  it('should return error if the account is already exists', async () => {
    const account_id = uuidv4();
    await createAccount(account_id, 100);
    const res = await createAccount(account_id, 200);
    expect(res.statusCode).toEqual(200);
    expect(res.body.is_success).toBe(false);
  });
});

describe("Getting accounts' balance", () => {
  it('should return a balance', async () => {
    const account_id = uuidv4();
    await createAccount(account_id, 1234.56);
    const res = await fetchBalance(account_id);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      is_success: true,
      balance: '1234.56'
    });
  });
  it('should return error if account is not exists', async () => {
    const account_id = uuidv4();
    const res = await fetchBalance(account_id);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      is_success: false
    });
  });
});

describe('Making transfers', () => {
  it('should make a transfer', async () => {
    const sender_id = uuidv4();
    const recipient_id = uuidv4();
    await createAccount(sender_id, 1000);
    await createAccount(recipient_id, 0);
    const res = await makeTransfer(sender_id, recipient_id, 100);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      is_success: true,
      sender_balance: '900.00',
      recipient_balance: '100.00'
    });
  });

  it('should return error if not enough money', async () => {
    const sender_id = uuidv4();
    const recipient_id = uuidv4();
    await createAccount(sender_id, 1000);
    await createAccount(recipient_id, 0);
    const res = await makeTransfer(sender_id, recipient_id, 2000);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      is_success: false,
      sender_balance: '1000.00',
      recipient_balance: '0.00'
    });
  });

  it('should return error if not existent account', async () => {
    const sender_id = uuidv4();
    const recipient_id = uuidv4();
    await createAccount(sender_id, 1000);
    const res = await makeTransfer(sender_id, recipient_id, 100);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      is_success: false,
      sender_balance: '1000.00',
      recipient_balance: null
    });
  });

  it('should return error if sender sends to themselves', async () => {
    const sender_id = uuidv4();
    await createAccount(sender_id, 1000);
    const res = await makeTransfer(sender_id, sender_id, 100);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      is_success: false,
      sender_balance: '1000.00',
      recipient_balance: '1000.00'
    });
  });

  it('should return error if amount is negative', async () => {
    const sender_id = uuidv4();
    const recipient_id = uuidv4();
    await createAccount(sender_id, 1000);
    await createAccount(recipient_id, 0);
    const res = await makeTransfer(sender_id, recipient_id, -100);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      is_success: false,
      sender_balance: '1000.00',
      recipient_balance: '0.00'
    });
  });

  it('should return error if amount is incorrect', async () => {
    const sender_id = uuidv4();
    const recipient_id = uuidv4();
    await createAccount(sender_id, 1000);
    await createAccount(recipient_id, 0);
    const res = await makeTransfer(sender_id, recipient_id, 'foo');
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      is_success: false
    });
  });

  it('should be ok with concurrency', async () => {
    const sender_id = uuidv4();
    const recipient_id = uuidv4();

    await createAccount(sender_id, 1000);
    await createAccount(recipient_id, 0);

    const promises = Array(100)
      .fill(0)
      .map(() => makeTransfer(sender_id, recipient_id, 1));

    await Promise.all(promises);

    const senderRes = await fetchBalance(sender_id);
    expect(senderRes.body).toEqual({
      is_success: true,
      balance: '900.00'
    });

    const recipientRes = await fetchBalance(recipient_id);
    expect(recipientRes.body).toEqual({
      is_success: true,
      balance: '100.00'
    });
  });
});
