const axios = require('axios');
const { v4: uuidv4 } = require('uuid');

const HOST = 'http://localhost:3000';
const REQUEST_COUNT = 1000;

async function main() {
  console.log('Benchmarking %d requests...', REQUEST_COUNT);

  const sender_id = uuidv4();
  const recipient_id = uuidv4();
  await axios.post(HOST + '/api/accounts', {
    account_id: sender_id,
    balance: 1000000000
  });
  await axios.post(HOST + '/api/accounts', {
    account_id: recipient_id,
    balance: 0
  });

  const makeTransfer = () =>
    axios.post(HOST + '/api/transfers', {
      sender_id,
      recipient_id,
      amount: 1
    });

  const startTime = Date.now();
  const promises = Array(REQUEST_COUNT).fill(0).map(makeTransfer);
  await Promise.all(promises);
  const endTime = Date.now();

  const time = endTime - startTime;
  const speed = Math.round((REQUEST_COUNT / time) * 1000);

  console.log('%d requests, %d ms, %d RPS', REQUEST_COUNT, time, speed);
}

main();
