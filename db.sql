DROP TABLE IF EXISTS accounts;
CREATE TABLE accounts (
  account_id uuid NOT NULL PRIMARY KEY,
  balance numeric(15, 2) NOT NULL,
  created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

DROP FUNCTION IF EXISTS make_transfer;
CREATE FUNCTION make_transfer (
  sender_id accounts.account_id%TYPE,
  recipient_id accounts.account_id%TYPE,
  amount accounts.balance%TYPE,
  OUT is_success bool,
  OUT sender_balance accounts.balance%TYPE,
  OUT recipient_balance accounts.balance%TYPE
) RETURNS record AS $$
BEGIN
  is_success := false;

  SELECT balance INTO sender_balance FROM accounts WHERE account_id = sender_id FOR UPDATE;
  SELECT balance INTO recipient_balance FROM accounts WHERE account_id = recipient_id FOR UPDATE;

  IF (
    sender_id <> recipient_id
    AND sender_balance IS NOT NULL
    AND recipient_balance IS NOT NULL
    AND amount > 0
    AND sender_balance >= amount
  ) THEN
    sender_balance := sender_balance - amount;
    recipient_balance := recipient_balance + amount;
    is_success := true;

    UPDATE accounts SET balance = sender_balance WHERE account_id = sender_id;
    UPDATE accounts SET balance = recipient_balance WHERE account_id = recipient_id;
  END IF;

END;
$$ LANGUAGE plpgsql;
