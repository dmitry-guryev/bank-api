require('dotenv').config();

const fs = require('fs');
const pool = require('./pool');

async function main() {
  console.log('Initializing Bank API DB...');
  const sql = fs.readFileSync('./db.sql').toString();
  await pool.query(sql);
  console.log('Done');
  pool.end();
}

main();
