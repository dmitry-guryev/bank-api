# Bank API

## Интерфейс API

### Создание счёта

```
POST /api/accounts
```

Body params:

- `account_id` – GUID создаваемого счёта
- `balance` – Стартовый баланс счёта (>= 0)

Response example:

```json
{
  "is_success": true
}
```

### Получение данных счёта

```
GET /api/accounts/:account_id
```

Response example:

```json
{
  "is_success": true,
  "balance": "100500.49"
}
```

### Перевод со счёта на счёт

```
POST /api/transfers
```

Body params:

- `sender_id` – GUID счёта отправителя
- `recipient_id` – GUID счёта получателя
- `amount` – Сумма перевода (> 0)

Response example:

```json
{
  "is_success": true,
  "sender_balance": "9900.00",
  "recipient_balance": "100.00"
}
```

## Файлы

- `db.sql` – структура базы и хранимая процедура для выполнения перевода
- `server.js` – исходный код сервера API
- `server.test.js` – тесты сервера API

## Установка

1.  Должна быть установлена актуальная версия Node.js и Yarn
2.  Установить модули: `yarn install`
3.  Установить postgresql, создать пользователя и БД
4.  Создать в корне проекта файл `.env` с конфигурацией БД, см. https://www.postgresql.org/docs/9.1/libpq-envars.html
5.  Выполнить `yarn init-db` для инициализации базы

Пример файла `.env`:

```
PGHOST=localhost
PGPORT=5432
PGDATABASE=bankapi
PGUSER=bankuser
PGPASSWORD=bankpass
```

## Запуск

```bash
yarn start
```

По умолчанию сервер запустится на `localhost:3000`

## Запуск тестов API

```bash
yarn test
```

Для тестов API сервер отдельно запускать не нужно.

## Запуск бенчмарка

Для запуска бенчмарка нужен запущенный сервер API.

Бенчмарк сейчас параллельно запускает 1000 конкурентных запросов на изменение одних и тех же счетов, и считает средний RPS.

```bash
yarn benchmark
```

Пример результата запуска:

```
Benchmarking 1000 requests...
1000 requests, 2285 ms, 438 RPS
```
