require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const pool = require('./pool');

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());

const port = 3000;

app.get('/', (req, res) => res.send('Hello World!'));

app.get('/api/accounts/:account_id', async (req, res) => {
  const { account_id } = req.params;
  try {
    const result = await pool.query(
      'SELECT balance FROM accounts WHERE account_id=$1',
      [account_id]
    );
    if (result.rows.length) {
      const balance = result.rows[0].balance;
      res.send({
        is_success: true,
        balance
      });
    } else {
      res.send({
        is_success: false
      });
    }
  } catch (e) {
    res.send({
      is_success: false
    });
  }
});

app.post('/api/accounts', async (req, res) => {
  const { account_id, balance } = req.body;
  if (balance < 0) {
    res.send({ is_success: false });
    return;
  }
  try {
    const result = await pool.query(
      'INSERT INTO accounts (account_id, balance) VALUES ($1, $2)',
      [account_id, balance]
    );
    res.send({
      is_success: true
    });
  } catch (e) {
    res.send({
      is_success: false
    });
  }
});

app.post('/api/transfers', async (req, res) => {
  const { sender_id, recipient_id, amount } = req.body;
  try {
    const result = await pool.query('SELECT * FROM make_transfer($1, $2, $3)', [
      sender_id,
      recipient_id,
      amount
    ]);
    res.send(result.rows[0]);
  } catch (e) {
    res.send({
      is_success: false
    });
  }
});

if (require.main === module) {
  app.listen(port, () =>
    console.log(`Server listening at http://localhost:${port}`)
  );
}

module.exports = app;
